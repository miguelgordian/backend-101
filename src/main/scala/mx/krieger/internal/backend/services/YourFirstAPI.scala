package mx.krieger.internal.backend.services

import com.google.api.server.spi.config.ApiMethod.HttpMethod
import com.google.api.server.spi.config.{Api, ApiMethod, AuthLevel}
import mx.krieger.internal.backend.services.wrappers.Saludo

@Api(name = "myFirstApi", description = "Mi primera Api", authLevel = AuthLevel.NONE, version = "v1")
class YourFirstAPI {
  @ApiMethod(name = "saludo", path="saluda", httpMethod = "GET", authLevel = AuthLevel.NONE)
  def hola() = new Saludo()
}
